﻿namespace YSLib
{
	namespace Products
	{
		namespace Trigrams
		{
			public abstract class Trigram
			{
				public BaseLine[] Lines;
				public abstract int Id { get; }
			}
			public class Quian : Trigram
			{
				public override int Id => 1;
			}
			public class Zhen : Trigram
			{
				public override int Id => 2;
			}
			public class Kan : Trigram
			{
				public override int Id => 3;
			}
			public class Gen : Trigram
			{
				public override int Id => 4;
			}
			public class Kun : Trigram
			{
				public override int Id => 5;
			}
			public class Xun : Trigram
			{
				public override int Id => 6;
			}
			public class Li : Trigram
			{
				public override int Id => 7;

			}
			public class Dui : Trigram
			{
				public override int Id => 8;
			}
		}
	}
}
