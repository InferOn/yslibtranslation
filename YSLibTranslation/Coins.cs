﻿using System;
using System.Collections.Generic;

namespace YSLib
{
	namespace Products
	{
		public static class CoinShot
		{
			public static ShotResult[] Run(int thrown)
			{
				var result = new List<ShotResult>();
				for (var i = 0; i < thrown; i++)
				{
					result.Add(new ShotResult(Coin.Toss()));
				}
				return result.ToArray();
			}
		}
		public class ShotResult
		{
			public ShotResult(int value)
			{
				Value = value;
			}
			public int Value { get; private set; }
		}
		public class Coin
		{
			private enum eYingYang
			{
				Ying = 2,
				Yang = 3
			}
			public static int Toss()
			{
				var randomNumber = new byte[1];
				var rnd = new Random();
				return rnd.Next(1, 2) % 2 == 0 ? (int)eYingYang.Ying : (int)eYingYang.Yang;
			}
		}
	}
}

